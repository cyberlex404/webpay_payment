<?php
/**
 * Contains \Drupal\webpay_payment\Plugin\Payment\MethodConfiguration\Webpay.
 */

namespace Drupal\webpay_payment\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationInterface;
use Drupal\payment_offsite_api\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBaseOffsite;


/**
 * Provides the configuration for the payment_webpay payment method plugin.
 *
 * Plugins extending this class should provide a configuration schema that
 * extends
 * plugin.plugin_configuration.payment_method_configuration.payment_webpay.
 *
 * @PaymentMethodConfiguration(
 *   id = "payment_webpay",
 *   label = @Translation("Webpay"),
 *   description = @Translation("A payment method type that process payments via Webpay.by payment gateway.")
 * )
 */
class Webpay extends PaymentMethodConfigurationBaseOffsite implements PaymentMethodConfigurationInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'message_text' => 'In addition to the order amount webpay fee can be charged.',
      'message_text_format' => 'plain_text',
      'ipn_statuses' => [
        'success' => 'payment_success',
        'fail' => 'payment_failed',
      ],
      'config' => [
        'wsb_storeid' => '',
        'secret_key' => '',
        'server' => 'test',
        'username' => '',
        'pass' => '',
      ],
    ];
  }

  /**
   * Implements a form API #process callback.
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {
    $element['wsb_storeid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Store id'),
      '#description' => $this->t('Your Webpay Store ID'),
      '#default_value' => $this->getStoreId(),
      '#required' => TRUE,
    ];

    $element['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#description' => $this->t('Secret key. Random key: @random_key', [
        '@random_key' => substr(Crypt::randomBytesBase64(), 0, 32),
      ]),
      '#maxlength' => 32,
      '#default_value' => $this->getSecretKey(),
      '#required' => TRUE,
    ];

    $element['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->getUsername(),
      '#required' => TRUE,
    ];

    $element['pass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#maxlength' => 32,
      '#default_value' => $this->getPassword(),
      '#required' => TRUE,
    ];
    $element['pass_hash'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hash password'),
      '#description' => $this->t('Select if update password value'),
    ];

    $element['server'] = [
      '#type' => 'radios',
      '#title' => $this->t('Webpay server'),
      '#options' => [
        'test' => $this->t('Test - use for testing.'),
        'live' => $this->t('Live - use for processing real transactions'),
      ],
      '#default_value' => $this->getServer(),
      '#required' => TRUE,
    ];

    return parent::processBuildConfigurationForm($element, $form_state, $form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['plugin_form']['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->setStoreId($values['wsb_storeid']);
    $this->setSecretKey($values['secret_key']);
    $this->setServer($values['server']);
    $this->setUsername($values['username']);
    $pass = ($values['pass_hash']) ? md5($values['pass']) : $values['pass'];
    $this->setPassword($pass);

  }

  /**
   * Gets the Store ID.
   *
   * @return string
   */
  public function getStoreId() {
    return $this->configuration['config']['wsb_storeid'];
  }

  /**
   * Sets the Store ID.
   *
   * @param string $storeId
   *
   * @return static
   */
  public function setStoreId($storeId) {
    $this->configuration['config']['wsb_storeid'] = $storeId;

    return $this;
  }

  /**
   * Gets the Secret key.
   *
   * @return string
   */
  public function getSecretKey() {
    return $this->configuration['config']['secret_key'];
  }

  /**
   * Sets the Secret key.
   *
   * @param string $secretKey
   *
   * @return static
   */
  public function setSecretKey($secretKey) {
    $this->configuration['config']['secret_key'] = $secretKey;

    return $this;
  }

  /**
   * Gets the username.
   *
   * @return string
   */
  public function getUsername() {
    return $this->configuration['config']['username'];
  }

  /**
   * Sets the username.
   *
   * @param string $username
   *
   * @return static
   */
  public function setUsername($username) {
    $this->configuration['config']['username'] = $username;

    return $this;
  }

  /**
   * Gets the password.
   *
   * @return string
   */
  public function getPassword() {
    return $this->configuration['config']['pass'];
  }

  /**
   * Sets the password.
   *
   * @param string $pass
   *
   * @return static
   */
  public function setPassword($pass) {
    $this->configuration['config']['pass'] = $pass;

    return $this;
  }


  /**
   * Gets the pass2.
   *
   * @return string
   */
  public function getServer() {
    return $this->configuration['config']['server'];
  }

  /**
   * Sets the server.
   *
   * @param string $server
   *
   * @return static
   */
  public function setServer($server) {
    $this->configuration['config']['server'] = $server;

    return $this;
  }

}
