<?php

/**
 * Contains \Drupal\webpay_payment\Plugin\Payment\Method\Webpay.
 */

namespace Drupal\webpay_payment\Plugin\Payment\Method;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimple;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimpleInterface;
use Drupal\webpay_payment\WebpayPaymentTypeInterface;
use Drupal\webpay_payment\WebpayTransactionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A webpay payment method.
 *
 * Plugins that extend this class must have the following keys in their plugin
 * definitions:
 * - entity_id: (string) The ID of the payment method entity the plugin is for.
 * - execute_status_id: (string) The ID of the payment status plugin to set at
 *   payment execution.
 *
 * @PaymentMethod(
 *   id = "payment_webpay",
 *   deriver = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteDeriver",
 *   operations_provider = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteOperationsProvider",
 *   capture = false
 * )
 */
class Webpay extends PaymentMethodOffsiteSimple implements PaymentMethodOffsiteSimpleInterface, ContainerFactoryPluginInterface, WebpayPaymentTypeInterface {

  /**
   * @var \Drupal\webpay_payment\WebpayTransactionInterface
   */
  protected $transaction;

  /**
   * Webpay constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param array $plugin_definition
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\payment\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\Core\Utility\Token $token
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition,
    ModuleHandlerInterface $module_handler,
    EventDispatcherInterface $event_dispatcher,
    Token $token,
    PaymentStatusManagerInterface $payment_status_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $event_dispatcher, $token, $payment_status_manager);
    $this->transaction = \Drupal::service('webpay_payment.transaction');
  }

  /**
   * Webpay server payment URL.
   */
  const SERVER_URL = 'https://payment.webpay.by';
  const TEST_SERVER_URL = 'https://securesandbox.webpay.by';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'brand_label' => '',
      'execute_status_id' => 'payment_pending',
      'capture' => FALSE,
      'capture_status_id' => 'payment_success',
      'refund' => FALSE,
      'refund_status_id' => 'payment_refunded',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * Gets the ID of the payment method this plugin is for.
   *
   * @return string
   */
  public function getEntityId() {
    return $this->pluginDefinition['entity_id'];
  }

  public function getServer() {
    return $this->pluginDefinition['config']['server'];
  }

  public function getAction() {
    return $this->getServer() == 'test' ? self::TEST_SERVER_URL : self::SERVER_URL;
  }

  public function getStoreId() {
    return $this->pluginDefinition['config']['wsb_storeid'];
  }

  public function getSecretKey() {
    return $this->pluginDefinition['config']['secret_key'];
  }

  public function getUsername() {
    return $this->pluginDefinition['config']['username'];
  }

  public function getPassword() {
    return $this->pluginDefinition['config']['pass'];
  }


  public function validateOrderNum() {
    if ($this->request->request->has('site_order_id')) {
      $order_num = $this->request->request->get('site_order_id');

      $paymentId = (int) substr($order_num, 6);
      $payment = \Drupal::entityTypeManager()
        ->getStorage('payment')
        ->load($paymentId);

      if ($payment instanceof PaymentInterface) {
        $this->setPayment($payment);
        return TRUE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getValidators() {
    return [
      'validateEmpty',
      'validateRequiredKeys',
      'validateOrderNum',
      'validateAmount',
      'validateSignature',
    //  'validateTransactionId',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentStatusAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettablePaymentStatuses(AccountInterface $account, PaymentInterface $payment) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getResultPages() {
    return [
      'success' => TRUE,
      'fail' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function paymentForm() {
    $form = array();
    $payment = $this->getPayment();
    $form['#action'] = $this->getAction();

    $this->addPaymentFormData('*scart', '');
    $this->addPaymentFormData('wsb_version', 2);
    $this->addPaymentFormData('wsb_language_id', 'russian');
    $this->addPaymentFormData('wsb_storeid', $this->getStoreId());
    //wsb_store
    $this->addPaymentFormData('wsb_order_num', $this->getOrderNum());
    $wsb_test = ($this->getServer() == 'test') ? '1': '0';
    $this->addPaymentFormData('wsb_test', $wsb_test);
    $this->addPaymentFormData('wsb_currency_id', $payment->getCurrencyCode());

    $seed = $this->getSeed();
    $this->addPaymentFormData('wsb_seed', $seed);
    $this->addPaymentFormData('wsb_signature', $seed);
    /** @var PaymentLineItemInterface[] $lineItems */
    $lineItems = $payment->getLineItems();

    $index = 0;
    foreach ($lineItems as $key => $lineItem) {
      $this->addPaymentFormData("wsb_invoice_item_name[$index]", $lineItem->getDescription());
      $this->addPaymentFormData("wsb_invoice_item_quantity[$index]", $lineItem->getQuantity());
      $this->addPaymentFormData("wsb_invoice_item_price[$index]", $this->amountFormat($lineItem->getAmount()));
      $index++;
    }
    // wsb_notify_url
    //$this->addPaymentFormData('wsb_notify_url', $this->getNotifyUrl()->toString());
    // wsb_return_url
    $this->addPaymentFormData('wsb_return_url', $this->getUrl('success')->toString());
    // wsb_cancel_return_url
    $this->addPaymentFormData('wsb_cancel_return_url', $this->getUrl('cancel')->toString());

    $this->addPaymentFormData('wsb_total', $this->amountFormat($payment->getAmount()));
    $this->addPaymentFormData('wsb_signature', $this->getSignature());

    $form += $this->generateForm();

    return $form;
  }


  public function amountFormat($amount) {
    return number_format(round($amount, 2), 2, '.', '');
  }

  public function getOrderNum() {
    return 'ORDER-' . $this->getPayment()->id();
  }


  /**
   * @return \Drupal\Core\Url
   */
  public function getNotifyUrl() {
    return Url::fromRoute('webpay_payment.notify', [
      'payment_method_configuration' => 'webpay',
    ], ['absolute' => TRUE]);
  }

  /**
   * @param $external_status
   *
   * @return \Drupal\Core\Url
   */
  public function getUrl($external_status) {
    $options = ['absolute' => TRUE];
    $url = Url::fromRoute('payment.offsite.external', [
      'payment_method_configuration' => 'webpay',
      'external_status' => $external_status,
    ], $options);
    return $url;
  }

  /**
   * {@inheritdoc}
   *
   * @todo refactor
   */
  public function getSignature($signature_type = PaymentMethodOffsiteSimple::SIGN_OUT) {

    $payment = $this->getPayment();

    $algo = 'sha1';

    if ($signature_type == PaymentMethodOffsiteSimple::SIGN_OUT) {

      $signature_data = [
        $payment->getCreatedTime(),
        $this->getStoreId(),
        $this->getOrderNum(),
        ($this->getServer() == 'test') ? '1': '0',
        $payment->getCurrency()->getCurrencyCode(),
        (string) $this->amountFormat(round($payment->getAmount(), 2)),
        $this->getSecretKey(),
      ];

    }else {
      $algo = 'md5';
      /** batch_timestamp currency_id  amount  payment_method  order_id  site_order_id  transaction_id  payment_type
      rrn  «Секретный ключ»  */
      $signature_data = [
        $this->request->request->get('batch_timestamp'),
        $this->request->request->get('currency_id'),
        $this->request->request->get('amount'),
        $this->request->request->get('payment_method'),
        $this->request->request->get('order_id'),
        $this->request->request->get('site_order_id'),
        $this->request->request->get('transaction_id'),
        $this->request->request->get('payment_type'),
        $this->request->request->get('rrn'),
        $this->getSecretKey(),
      ];
    }

    $this->logger->debug(
      'getSignature' . $signature_type . ':= '
      . hash($algo, implode('', $signature_data)). ' data:'
      . implode('', $signature_data)
    );
    // Calculate signature.
    return hash($algo, implode('', $signature_data));
  }

  /**
   * Return local payment status related to given remote.
   *
   * @param string $status
   *   Remote status id.
   *
   * @return string.
   *   Local status id.
   */
  public function getStatusId($status) {
    return $this->pluginDefinition['ipn_statuses'][$status];
  }

  /**
   * {@inheritdoc}
   */
  public function ipnExecute() {
    if (!$this->ipnValidate()) {
      return [
        'status' => 'fail',
        'message' => 'bad data',
        'response_code' => 200,
      ];
    }

    $transaction_id = $this->request->request->get('transaction_id');

    if (!is_null($transaction_id)) {

      $transaction = $this->transaction->getTransaction($this->getUsername(), $this->getPassword(), $transaction_id, $this->getServer());
      \Drupal::logger('payment debug transaction')->debug(Json::encode($transaction));
      \Drupal::logger('payment debug transaction post ')->debug(Json::encode($this->request->request->all()));
      $paymentTypeId = $this->request->request->get('payment_type');
      $payment_status = $this->getStatusByPaymentType($paymentTypeId);
      $status = isset($payment_status) ? $payment_status : 'payment_pending';
      $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($status));
      $this->getPayment()->save();
    }

    return [
      'status' => 'success',
      'message' => 'OK' . $this->getPayment()->id(),
      'response_code' => 200,
    ];
  }

  /**
   * Returns success message.
   *
   * @return array
   *   The renderable array.
   */
  public function getSuccessContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', array('@external_status' => 'success')),
    ];
  }

  /**
   * Returns fail message.
   *
   * @return array
   *   The renderable array.
   */
  public function getFailContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', array('@external_status' => 'FAIL')),
    ];
  }

  public function getSeed() {
    return $this->getPayment()->getCreatedTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantIdName() {
    return 'wsb_storeid'; // todo: do not use
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionIdName() {
    return 'transaction_id';
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountName() {
    return 'amount';
  }

  /**
   * {@inheritdoc}
   */
  public function getSignatureName() {
    return 'wsb_signature';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    $required_keys = [
      'batch_timestamp',
      'currency_id',
      'amount',
      'payment_method',
      'order_id',
      'site_order_id',
      'transaction_id',
      'payment_type',
      'rrn',
      'wsb_signature',
    ];
    return $required_keys;
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured() {
    return !empty($this->getAction());
  }


  private function getStatusByPaymentType($paymentType) {
    $id = (int) $paymentType;
    $status = [
      self::PAYMENT_TYPE_COMPLETED => 'payment_success',
      self::PAYMENT_TYPE_AUTHORIZED => 'payment_success',
      self::PAYMENT_TYPE_PENDING => 'payment_pending',
      self::PAYMENT_TYPE_FAILED => 'payment_failed',
      self::PAYMENT_TYPE_REFUNDED => 'payment_refunded',
    ];
    return $status[$id];
  }

  protected function doCapturePayment() {}


}
