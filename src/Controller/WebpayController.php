<?php

namespace Drupal\webpay_payment\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\payment\Entity\PaymentMethodConfigurationInterface;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodManagerInterface;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimpleInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WebpayController.
 */
class WebpayController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The payment method manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Method\PaymentMethodManagerInterface
   */
  protected $paymentMethodManager;

  /**
   * Constructs a new WebpayController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\payment\Plugin\Payment\Method\PaymentMethodManagerInterface $payment_method_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, PaymentMethodManagerInterface $payment_method_manager) {
    $this->configFactory = $config_factory;
    $this->paymentMethodManager = $payment_method_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.payment.method')
    );
  }

  /**
   * Dev controller
   * @deprecated dev method
   *
   * @return mixed
   *   Return Hello string.
   */
  public function hello($name, Request $request) {

    $pass = $request->query->get('pass');
    $username = $request->query->get('username');
    $t[] = $this->getTransaction('623241774', $username, $pass);
    $t[] = $this->getTransaction('812830675', $username, $pass);
    $t[] = $this->getTransaction('487282852', $username, $pass);

    dpm($t);

    $data = [
      'name' => $name,
      'query' => $request->query->all(),
      'request' => $request->request->all()
    ];



    \Drupal::logger('payment debug')->debug(Json::encode($data));
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: hello with parameter(s): $name'),
    ];
  }


  public function getTransaction($transaction_id, $username, $pass) {

    $transaction = [];
    $client = \Drupal::httpClient();


    $xmldata = '<?xml version="1.0" encoding="ISO-8859-1" ?><wsb_api_request><command>get_transaction</command><authorization>' .
      "<username>$username</username>" .
      "<password>$pass</password>" .
      "</authorization><fields><transaction_id>$transaction_id</transaction_id></fields></wsb_api_request>";

    $data = '*API=&API_XML_REQUEST='.urlencode($xmldata);

    try {
      $response = $client->post('https://sandbox.webpay.by/', [
        'curl' => [
          CURLOPT_HTTPHEADER => [],
          CURLOPT_POSTFIELDS => $data
        ]
      ]);

      if ($response instanceof ResponseInterface && $response->getStatusCode() == 200) {

        $xmlString = $response->getBody()->getContents();
        $xml = new \SimpleXMLElement($xmlString);

        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        $transaction = $array['fields'];
      }
    }catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }


    return $transaction;
  }


  public function notify(PaymentMethodConfigurationInterface $payment_method_configuration, Request $request) {

    $plugin_id = $payment_method_configuration->getPluginId() . ':' . $payment_method_configuration->id();

    /** @var PaymentMethodOffsiteSimpleInterface $payment_method */
    $payment_method = $this->paymentMethodManager
      ->createInstance($plugin_id, $payment_method_configuration->getPluginConfiguration());

    $ipn_result = $payment_method->ipnExecute();


    $response = new JsonResponse();
    $data = [
      'ipn' => $ipn_result,
      'query' => $request->query->all(),
      'request' => $request->request->all()
    ];

    \Drupal::logger('payment debug notify')->debug(Json::encode($data));

    $response->setData([
      'status' => 'success',
      'code' => 200,
      'data' => $data,
    ]);

    return $response;
  }

}
