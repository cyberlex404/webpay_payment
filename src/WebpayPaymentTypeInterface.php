<?php


namespace Drupal\webpay_payment;


interface WebpayPaymentTypeInterface {

  /**
   * 1 Completed Завершенная
  2 Declined Отклоненная
  3 Pending В обработке
  4 Authorized Авторизованная
  5 Refunded Возвращенная
  6 System Системная
  7 Voided Сброшенная после авторизации
  8 Failed Ошибка в проведении операции
  9 Partial Voided Частичный сброс
  10 Recurrent Рекуррентный платеж
   */
  const PAYMENT_TYPE_COMPLETED = 1;
  const PAYMENT_TYPE_DECLINED = 2;
  const PAYMENT_TYPE_PENDING = 3;
  const PAYMENT_TYPE_AUTHORIZED = 4;
  const PAYMENT_TYPE_REFUNDED = 5;
  const PAYMENT_TYPE_SYSTEM = 6;
  const PAYMENT_TYPE_VOIDED = 7;
  const PAYMENT_TYPE_FAILED = 8;
  const PAYMENT_TYPE_PARTIAL_VOIDED = 9;
  const PAYMENT_TYPE_RECURRENT = 10;
}