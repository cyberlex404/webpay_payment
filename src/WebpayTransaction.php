<?php

namespace Drupal\webpay_payment;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class WebpayTransaction.
 */
class WebpayTransaction implements WebpayTransactionInterface, WebpayPaymentTypeInterface {

  const SERVER_URL = 'https://billing.webpay.by/';
  const TEST_SERVER_URL = 'https://sandbox.webpay.by/';
  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new WebpayTransaction object.
   */
  public function __construct(ClientInterface $http_client, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransaction($username, $pass, $transaction_id, $server) {

    $transaction = [];
    $xmldata = '<?xml version="1.0" encoding="ISO-8859-1" ?><wsb_api_request><command>get_transaction</command><authorization>' .
      "<username>$username</username>" .
      "<password>$pass</password>" .
      "</authorization><fields><transaction_id>$transaction_id</transaction_id></fields></wsb_api_request>";
    $data = '*API=&API_XML_REQUEST='.urlencode($xmldata);

    try {
      $response = $this->httpClient->request('POST', $this->getServer($server), [
        'curl' => [
          CURLOPT_HTTPHEADER => [],
          CURLOPT_POSTFIELDS => $data
        ]
      ]);

      if ($response instanceof ResponseInterface && $response->getStatusCode() == 200) {
        $xmlString = $response->getBody()->getContents();
        $xml = new \SimpleXMLElement($xmlString);

        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        $transaction = $array['fields'];
      }
    }catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
      return NULL;
    }

    return $transaction;
  }

  /**
   * @param $server
   *
   * @return string
   *  Billing server url
   */
  private function getServer($server) {
    return $server == 'test' ? self::TEST_SERVER_URL : self::SERVER_URL;
  }
}
