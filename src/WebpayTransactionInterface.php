<?php

namespace Drupal\webpay_payment;

/**
 * Interface WebpayTransactionInterface.
 */
interface WebpayTransactionInterface {

  /**
   * @param string $username
   *   Username in webpay.by
   * @param string $pass
   * @param string $transaction_id
   * @param string $server
   *
   * @return mixed
   */
  public function getTransaction($username, $pass, $transaction_id, $server);

}
